const express = require('express')
const router = express.Router()


router.use('/maint',  require('./routes'))
router.use(require('./activityRecorder'))

module.exports = router