const express = require("express")
const router = express.Router()
const activity = require('./activityQueue')

router.get('/ping', (req, res, next) => {
  return res.json({'message': 'pong'})
})

router.get('/activity', (req, res, next) => {
  return res.json({'activity': activity.getActivityCount()})
})

module.exports = router