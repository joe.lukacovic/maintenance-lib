var activityQueue = []
var lastClear = new Date()
setInterval(() => {  
  activityQueue = activityQueue.filter(a => a.getTime() > lastClear.getTime())
  lastClear = new Date()
}, 1000)

module.exports = {
  setActivity: () => {
    activityQueue.push(new Date())
  },
  getActivityCount: () => {
    return activityQueue.length
  }
}