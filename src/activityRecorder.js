const activity = require('./activityQueue')

module.exports = (req, res, next) => {
  activity.setActivity()
  next()
}